# Pijas y fijas

Un juego muy conocido es picas y fijas, el cual consiste en tratar de adivinar
un número en la menor cantidad de intentos. En cada intento, el jugador dice 4
dígitos (no repetidos) y el oponente le da pistas de cuántos aciertos tuvo, sin
indicarle cuales fueron, de la siguiente forma: Si algún dígito que dice el
jugador se encuentra dentro del número a adivinar, pero no está en la posición
correcta, se llama PICA.

Si el dígito se encuentra en la posición adecuada, se llama FIJA. Así, las
pistas serán la cantidad de PICAS y la cantidad de FIJAS que se tienen. El
juego termina cuando algún jugador tiene 4 FIJAS (4 dígitos en el orden
adecuado).

1. Haga una función que reciba un arreglo de 4 dígitos y un número X y que
   retorne verdadero o falso, si el número X está dentro del arreglo o no.

   ## Función: `estaEnNums`

   ### Entradas:

   - `nums`: arreglo de 4 dígitos.
   - `x`: número a detectar en arreglo `nums`.

   ### Salidas:

   - Si el número `x` está dentro del arreglo o no.

   ### Pseudo-código:

   1. Para `i` desde cero; hasta `i` menor que `4`; incremento `i` en `1`:
      1. Si número `x` es igual a elemento en posición `i` del arreglo `nums`:
         1. Retorno verdadero
   1. Retorno falso

1. Haga una función que reciba un arreglo de 4 posiciones enteras y lo llene
   con 4 dígitos no repetidos. Los dígitos se le deben pedir al usuario, pero
   la función debe validar que el usuario no ingrese números repetidos.

   ## Función: `iniNums`

   ### Entradas:

   - `nums`: arreglo de 4 posiciones enteras.

   ### Salidas:

   - Ninguna (referencia).

   ### Pseudo-código:

   1. Para `i` desde `0`; hasta `i` menor que `4`; incremento `i` en `1`:
      1. Asigno al elemento en la posición `i` del arreglo `nums` el valor `-1`.
   1. Para `i` desde `0`; hasta `i` menor que `4`; incremento `i` en `1`:
      1. Declaro variable `x` para almacenar dígito ingresado por usuario.
      1. Declaro variable `valido` como bandera para validación de dígito.
      1. Hacer:
         1. Asigno valor verdadero a variable `valido`.
         1. Pregunto a usuario por dígito No. `i + 1`.
         1. Obtengo de usuario dígito y asigno valor a `x`.
         1. Si valor de `x` es mayor a `9`:
            1. Imprimo error: dígito debe estar entre `0` y `9`.
            1. Asigno falso a variable `valido`.
         1. De lo contrario, si digito ya se encuentra en arreglo:
            1. Imprimo error: no se pude repetir dígito.
            1. Asigno falso a variable `valido`.
      1. Mientras dígito no es válido.
      1. Asigno a elemento en posición `i` de arreglo `nums` el valor de `x`.

1. Haga una función que reciba dos arreglos (el primero corresponde al número
   secreto y el segundo a la jugada de un jugador) y retorne la cantidad de
   PICAS

   ## Función: `contarPicas`

   ### Entradas:

   - `secr`: arreglo con número secreto de 4 dígitos.
   - `nums`: arreglo con número de 4 dígitos ingresado por usuario.

   ### Salidas:

   - `picas`: cantidad de dígitos coincidentes en posiciones distintas.

   ### Pseudo-código:

   1. Declaro variable `picas` e inicializo a `0`.
   1. Para `i` desde `0`; hasta `i` menor a `4`; incremento `i` en `1`:
      1. Para `j` desde `0`; hasta `j` menor a `4`; incremento `j` en `1`:
         1. Si `i` es diferente de `j` y elemento en posición `i` de arreglo
            `nums` es igual a elemento en posición `j` de arreglo `secr`:
            1. Icremento `picas` en `1`.
   1. Retorno `picas`.

1. Haga una función que reciba dos arreglos (el primero corresponde al número
   secreto y el segundo a la jugada de un jugador) y retorne la cantidad de
   FIJAS

   ## Función: `contarFijas`

   ### Entradas:

   - `secr`: arreglo con número secreto de 4 dígitos.
   - `nums`: arreglo con número de 4 dígitos ingresado por usuario.

   ### Salidas:

   - `fijas`: cantidad de dígitos coincidentes en posiciones iguales.

   ### Pseudo-código:

   1. Declaro variable `fijas` e inicializo a `0`.
   1. Para `i` desde `0`; hasta `i` menor a `4`; incremento `i` en `1`:
      1. Si elemento en posición `i` de arreglo `nums` es igual a elemento en
         posición `i` de arreglo `secr`:
          1. Icremento `fijas` en `1`.
   1. Retorno `fijas`.

1. Haga un programa que permita que un jugador ingrese un número de 4 dígitos
   no repetidos y que permita que otro jugador ingrese posibles jugadas, hasta
   cuando adivine el número secreto. El programa finalizará cuando se adivine
   el número o cuando el usuario no desee continuar. El algoritmo deberá
   mostrar la cantidad de jugadas realizadas.

   ## Función: `main`

   ### Entradas:

   - Ninguna.

   ### Salidas:

   - `EXIT_SUCCESS`.

   ### Psedo-código

   1. Declaro arreglo `secreto` de cuatro elementos.
   1. Declaro arreglo `numeros` de cuatro elementos.
   1. Declaro variable `salir` para almacenar intención de jugador 2 de
      finalizar juego.
   1. Declaro variable `fijas`.
   1. Declaro variable `jugadas` y asigno valor a `0`.
   1. Pregunto a jugador 1 por 4 dígitos secretos.
   1. Invoco `iniNums()` para guardar dígitos validados en arreglo `secreto`.
   1. Hacer:
      1. Pregunto a jugador 2 por finalización de juego.
      1. Obtengo de jugador 2 valor a asignar a `salir`.
      1. Si `salir` es diferente al caracter 's':
         1. Incremento `jugadas` en `1`.
         1. Imprimo jugada actual.
         1. Pregunto a jugador 2 por 4 dígitos.
         1. Invoco `iniNums()` para guardar dígitos validados en arreglo
            `numeros`.
         1. Invoco `contarPicas()` e imprimo resultado en pantalla.
         1. Invoco `contarFijas()`, asigno valor a `fijas` e imprimo resultado
            en pantalla.
   1. Mientras `salir` es diferente de 's' y `fijas` sea diferente de `4`.
   1. Si `salir` es diferente de 's':
      1. Imprimo victoria de jugador 2.
   1. De lo contrario:
      1. Imprimo victoria de jugador 1.
   1. Imprimo cantidad de jugadas.
