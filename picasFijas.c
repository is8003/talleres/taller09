#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>


bool estaEnNum(unsigned nums[], unsigned x);
void iniNums(unsigned nums[]);
unsigned contarPicas(unsigned secr[], unsigned nums[]);
unsigned contarFijas(unsigned secr[], unsigned nums[]);

/* Función: estaEnNum
 * ### Entradas:
 * - nums: arreglo de 4 dígitos.
 * - x: número a detectar en arreglo nums.
 * ### Salidas:
 * - Si el número x está dentro del arreglo o no.
 */
bool estaEnNum(unsigned nums[], unsigned x){
	for (size_t i = 0; i < 4; i++){
		if ( x == nums[i]){
			return true;
		}
	}

	return false;
}

/* Función: iniNums
 * ### Entradas:
 * - `nums`: arreglo de 4 posiciones enteras.
 * ### Salidas:
 * - Ninguna (referencia).
 */
void iniNums(unsigned nums[]){
	for ( size_t i = 0; i < 4; i++){
		unsigned x;
		bool valido;

		do{
			valido = true;

			printf("Ingresa dígito No. %lu: ", i + 1);
			scanf("%u", &x);

			if (x > 9){
				puts("Error: digito debe estar entre 0 y 9");
				valido = false;
			}
			else if (estaEnNum(nums, x)){
				puts("Error: no se puede repetir dígito");
				valido = false;
			}

		}while(!valido);

		nums[i] = x;
	}
}

/* Función: contarPicas
 * ### Entradas:
 * - secr: arreglo con número secreto de 4 dígitos.
 * - nums: arreglo con número de 4 dígitos ingresado por usuario.
 * ### Salidas:
 * - picas: cantidad de dígitos coincidentes en posiciones distintas.
 */
unsigned contarPicas(unsigned secr[], unsigned nums[]){
	unsigned picas = 0;
	for (size_t i = 0; i < 4; i++){
		for (size_t j = 0; j < 4; j++){
			if (i != j && nums[i] == secr[j]){
				picas++;
			}
		}
	}
	return picas;
}

/* Función: contarFijas
 * ### Entradas:
 * - secr: arreglo con número secreto de 4 dígitos.
 * - nums: arreglo con número de 4 dígitos ingresado por usuario.
 * ### Salidas:
 * - fijas: cantidad de dígitos coincidentes en posiciones distintas.
 */
unsigned contarFijas(unsigned secr[], unsigned nums[]){
	unsigned fijas = 0;
	for (size_t i = 0; i < 4; i++){
			if (nums[i] == secr[i]){
				fijas++;
			}
		}
	return fijas;
}


int main(void){
	unsigned secreto[4];
	unsigned numeros[4];
	char salir;
	unsigned fijas;
	unsigned jugadas = 0;

	puts("Jugador 1 - Ingrese secreto: ");
	iniNums(secreto);

	do{
		printf("%s", "Jugador 2 - ¿desea finalizar la partida? (s|n): ");
		scanf(" %c", &salir);

		if (salir != 's'){
			jugadas++;
			printf("Jugada No.: %u\n",jugadas);
			puts("Jugador 2 - Ingrese 4 dígitos: ");
			iniNums(numeros);
			printf("Picas: %u\n", contarPicas(secreto, numeros));
			printf("Fijas: %u\n", fijas = contarFijas(secreto, numeros));
		}
		
	}while (salir != 's' && fijas != 4);

	if (salir != 's'){
		puts("Jugador 2 - !Ganaste!");
	}
	else{
		puts("Jugador 1 - !Ganaste!");
	}

	printf("Cantidad de jugadas: %u\n", jugadas);

	exit(EXIT_SUCCESS);
}
